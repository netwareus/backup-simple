const { getManagerDrive } = require("./src/driveList");
const { runBackUp } = require("./src/backupManager");

module.exports = { getManagerDrive, runBackUp };