const assert = require("assert");
const {getNameDaily, getNameWeekly, getWeekNumber, getMonthName } = require("../src/calcName");

describe("week utility helpers", () =>{
    it("week numbers", ()=> {
        let dateTarget = new Date(2018,11,24,10,23,33);
        let weekNum = getWeekNumber(dateTarget);
        assert.equal(weekNum,52);

        dateTarget = new Date(2018,2,30,10,23,33);
        weekNum = getWeekNumber(dateTarget);
        assert.equal(weekNum,13);

        dateTarget = new Date(2018,3,2,10,23,33);
        weekNum = getWeekNumber(dateTarget);
        assert.equal(weekNum,14);

        dateTarget = new Date(2018,3,9,10,23,33);
        weekNum = getWeekNumber(dateTarget);
        assert.equal(weekNum,15);

        dateTarget = new Date(2018,3,1,10,23,33);
        weekNum = getWeekNumber(dateTarget);
        assert.equal(weekNum,13);

        dateTarget = new Date(2018,2,31,10,23,33);
        weekNum = getWeekNumber(dateTarget);
        assert.equal(weekNum,13);
    });
});
describe("calc name monthly", () => {
    it("2018 january", () => {
        const result = getMonthName(new Date(2018,0,3,12,3,2));
        assert.equal(result, "2018_01");
    });

    it("2018 may", () => {
        const result = getMonthName(new Date(2018,4,3,12,3,2));
        assert.equal(result, "2018_05");
    });

    it("2018 oct", () => {
        const result = getMonthName(new Date(2018,9,3,12,3,2));
        assert.equal(result, "2018_10");
    });

    it("2018 dec", () => {
        const result = getMonthName(new Date(2018,11,31,12,3,2));
        assert.equal(result, "2018_12");
    });
});
describe("calc name weekly", () => {
    it("date 3-2-2018 week 1", ()=> {
        const result =  getNameWeekly(new Date(2018,2,2,9,23,33));
        assert.equal(result.name, "2018_03_week01");
        assert.equal(result.deletePattern, "_week01");
    });
    it("date 3-5-2018 week 2", ()=> {
        const result =  getNameWeekly(new Date(2018,2,5,9,23,33));
        assert.equal(result.name, "2018_03_week02");
        assert.equal(result.deletePattern, "_week02");
    });
    it("date 2-28-2018 week 5", ()=> {
        const result =  getNameWeekly(new Date(2018,1,28,10,23,33));
        assert.equal(result.name, "2018_02_week05");
        assert.equal(result.deletePattern, "_week05");
    });

    it("date 3-30-2018 week 5", ()=> {
        const result =  getNameWeekly(new Date(2018,2,30,10,23,33));
        assert.equal(result.name, "2018_03_week05");
        assert.equal(result.deletePattern, "_week05");
    });

    it("date 3-31-2018 week 5", ()=> {
        const result =  getNameWeekly(new Date(2018,2,31,10,23,33));
        assert.equal(result.name, "2018_03_week05");
        assert.equal(result.deletePattern, "_week05");
    });
});
describe("calc name daily", () => {
    it("date 3-5-2018 monday", ()=> {
        const result =  getNameDaily(new Date(2018,2,5,9,23,33));
        assert.equal(result.name, "2018_03_05_monday");
    });
    it("date 3-6-2018 tuesday", ()=> {
        const result =  getNameDaily(new Date(2018,2,6,9,23,33));
        assert.equal(result.name, "2018_03_06_tuesday");
    });
    it("date 3-7-2018 wednesday", ()=> {
        const result =  getNameDaily(new Date(2018,2,7,9,23,33));
        assert.equal(result.name, "2018_03_07_wednesday");
    });
    it("date 3-8-2018 thursday", ()=> {
        const result =  getNameDaily(new Date(2018,2,8,9,23,33));
        assert.equal(result.name, "2018_03_08_thursday");
    });
    it("date 3-9-2018 friday", ()=> {
        const result =  getNameDaily(new Date(2018,2,9,9,23,33));
        assert.equal(result.name, "2018_03_09_friday");
    });
    it("date 3-10-2018 saturday", ()=> {
        const result =  getNameDaily(new Date(2018,2,10,9,23,33));
        assert.equal(result.name, "2018_03_10_saturday");
    });

    it("date 3-11-2018 sunday", ()=> {
        const result =  getNameDaily(new Date(2018,2,11,9,23,33));
        assert.equal(result.name, "2018_03_11_sunday");
    });

    it("date 3-12-2018 monday", ()=> {
        const result =  getNameDaily(new Date(2018,2,12,9,23,33));
        assert.equal(result.name, "2018_03_12_monday");
    });
});
