const { getBackUpFolders }  = require("../src/backupManager");
const assert = require("assert");
describe("basic backup", () =>{
    /*
    it("back up 3/2/2018", ()=> {
        const result = runBackUp(new Date(2018,2,2,12,3,2));
        assert(result !== null);
    });
    */
    it("get backup files names (set)", ()=>{
        const result = getBackUpFolders(new Date(2018,2,2,12,3,2), "tempName");
        const expected = {
            dayDir: "/tempName/2018_03_02_friday",
            dayDeletePattern: "_friday",
            weekDir: "/tempName/2018_03_week01",
            weekDeletePattern: "_week01",
            monthDir:"/tempName/2018_03"
        };
        assert.deepEqual(result, expected);
    });
});
