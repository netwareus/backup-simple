const assert = require("assert");
const { readFiles, writeFiles, deleteFiles } = require("../src/fileIo");

describe("read Files", ()=>{
   it("all in dir", ()=>{
        return readFiles("./test/testFiles/source/").then(files =>{
            assert.equal(files.length, 5);
        });
    });
    it("txt in dir", ()=>{
        return readFiles("./test/testFiles/source/", /.*\.(txt)/).then(files =>{
            assert.equal(files.length, 3);
        });
    });
    it("read directory  List", () =>{
        return readFiles("./test/testFiles/", "destination || source").then(files =>{
            assert.equal(files.length, 2);
        });
    });
});

describe("write Files", ()=> {
    const sourceFiles = ["./test/testFiles/source/one.txt", "./test/testFiles/source/two.txt"];
    const destinationFiles = ["./test/testFiles/destination/one.txt", "./test/testFiles/destination/two.txt"];
    before( async function() {
        await deleteFiles(destinationFiles);
    });

    after(async function() {
        await deleteFiles(destinationFiles);
    });

    it("write", async ()=>{
        writeFiles(sourceFiles, destinationFiles);
        assert(true);
        const fileCount = await readFiles("./test/testFiles/destination");
        assert(fileCount.length, destinationFiles.length);
    });
});