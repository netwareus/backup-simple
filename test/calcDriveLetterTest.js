const assert = require("assert");
const { getRecommendedDrive } = require("../src/driveList");
describe("get drive letter from raw",() => {
    const rawList = [{"enumerator":"SCSI","busType":"INVALID","busVersion":"2.0","device":"\\\\.\\PhysicalDrive0","raw":"\\\\.\\PhysicalDrive0","description":"NVMe SAMSUNG MZFLV128","error":null,"size":128035676160,"blockSize":512,"logicalBlockSize":512,"mountpoints":[{"path":"C:\\"}],"isReadOnly":false,"isSystem":true,"isVirtual":false,"isRemovable":false,"isCard":false,"isSCSI":true,"isUSB":false,"isUAS":false},{"enumerator":"USBSTOR","busType":"USB","busVersion":"2.0","device":"\\\\.\\PhysicalDrive1","raw":"\\\\.\\PhysicalDrive1","description":"SanDisk Cruzer Glide USB Device","error":null,"size":16008609792,"blockSize":512,"logicalBlockSize":512,"mountpoints":[{"path":"D:\\"}],"isReadOnly":false,"isSystem":false,"isVirtual":false,"isRemovable":true,"isCard":false,"isSCSI":false,"isUSB":true,"isUAS":false}];
    it("get drive", ()=>{
        const result = getRecommendedDrive(rawList);
        assert.equal(result.drive, "D:\\");
        assert(result.hasResult);
    });
});