const driveList = require('drivelist');

const getRecommendedDrive = (rawList)=> {
    const nonSys = rawList.filter((drive) => {
        return drive.isSystem === false && drive.isReadOnly === false
            && drive.busType === "USB" && drive.size > 10000;
    });
    console.log(`drive : ${process.env.BACKUP_DRIVE}`);
    const found = process.env.BACKUP_DRIVE || (nonSys.length > 0 ? nonSys[0].mountpoints[0].path : "");
    const defaultFolder = process.env.BACKUP_FOLDER || "/backup";
    return { hasResult: nonSys.length > 0, drive:  found, defaultFolder};
};

const getRawDriveList = () => {
    return new Promise((resolve, reject) => {
        driveList.list((error, drives) => {
            if (error)
                reject(error);
            else
                resolve(drives);
        });
    });
};

const getManagerDrive = async () => {
    const rawList = await getRawDriveList();
    return getRecommendedDrive(rawList);
};

module.exports = { getRecommendedDrive, getManagerDrive };