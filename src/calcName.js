const days =[ "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" ];

const getNameDaily = (date) => {
    const result = `${date.getFullYear()}_${date.getMonth() + 1}_${date.getDate()}_${days[date.getDay()]}`;
    return {
        "name" : result.replace(/(^|\D)(\d)(?!\d)/g, '$10$2'),
        "deletePattern" : `_${days[date.getDay()]}`.replace(/(^|\D)(\d)(?!\d)/g, '$10$2')
    };
};

const getNameWeekly = (date) => {
    const weekTag = `_week${getWeekInMonthNumber(date)}`.replace(/(^|\D)(\d)(?!\d)/g, '$10$2');
    const result = `${date.getFullYear()}_${date.getMonth() + 1}${weekTag}`;
    return {
        "name": result.replace(/(^|\D)(\d)(?!\d)/g, '$10$2'),
        "deletePattern": weekTag
    };
};

const getWeekNumber = (dateIn)=>{
    // Copy date so don't modify original
    dateIn = new Date(Date.UTC(dateIn.getFullYear(), dateIn.getMonth(), dateIn.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    dateIn.setUTCDate(dateIn.getUTCDate() + 4 - (dateIn.getUTCDay()||7));
    // Get first day of year
    const yearStart = new Date(Date.UTC(dateIn.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    return Math.ceil(( ( (dateIn - yearStart) / 86400000) + 1)/7);
};

const getWeekInMonthNumber = (date)=>{
    const currentWeek = getWeekNumber(date);
    const lastDayOfLastMonth = new Date(date.getFullYear(), date.getMonth(), 1);
    lastDayOfLastMonth.setDate(0);
    const lastWeekOfLastMonth = getWeekNumber(lastDayOfLastMonth);
    return currentWeek - lastWeekOfLastMonth + 1;
};

const getMonthName = (date) => {
    const result = `${date.getFullYear()}_${date.getMonth() + 1}`;
    return result.replace(/(^|\D)(\d)(?!\d)/g, '$10$2');
};

module.exports = {  getNameDaily, getNameWeekly, getWeekNumber, getMonthName };