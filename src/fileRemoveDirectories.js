const { W_OK, access, rmdir } = require("fs");
const { readFiles, deleteFiles } = require("./fileIo");

const deleteFilesInDirectory = async (directory) => {
    const files = await readFiles(directory);
    const filesFullPath = files.map((file)=> `${directory}/${file}`);
    await deleteFiles(filesFullPath);
};

const removeDirectories = (directories) => {
    let errorCount = 0;
    let completeCount = 0;
    return new Promise((resolve, reject) => {
        const checkComplete = (err) =>{
            if(err) errorCount++;
            completeCount++;
            if(completeCount === directories.length) {
                if (errorCount === 0)
                    resolve();
                else
                    reject("error deleting directories");
            }
        };

        directories.forEach( async (directory) =>{
            await deleteFilesInDirectory(directory);
            access(directory, W_OK, (err) => {
                if(err) {
                    if(err.code === "ENOENT") //does not exist
                        checkComplete();
                    else {
                        console.log(err);
                        checkComplete(err);
                    }
                }
                else
                    rmdir(directory, checkComplete);
            });
        });
        if(directories.length === 0)
            resolve();
    });
};

module.exports = {removeDirectories};