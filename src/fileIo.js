const { readdir, mkdir,createReadStream, createWriteStream, unlink, W_OK, access } = require("fs");

const readFiles = (source, pattern) => {
    return new Promise((resolve,reject) => {
        readdir(source, function(err, items) {
            if(err && err.code === "ENOENT")
                resolve([]);
            else if(err)
                reject(err);
            else if(pattern)
                resolve(items.filter((file) => file.match(pattern)));
            else
                resolve(items);
        });
    });
};

const createDir = (directory)=>{
    return new Promise((resolve, reject) =>{
        mkdir(directory,(err) => {
            if(err && err.code === "EEXIST")
                resolve();
            else if(err)
                reject(err);
            else
                resolve();
        });
    });
};

const writeFiles = (fileSources, fileDestinations) =>{
    return new Promise((resolve, reject) => {
        let countComplete = 0;
        let countErrors = 0;
        function complete(err, file) {
            if(err)
                countErrors++;
            countComplete++;
            console.log("complete " + file);
            if(countComplete === fileSources.length) {
                if (countErrors === 0)
                    resolve();
                else
                    reject("error writing directories");
            }
        }
        fileSources.forEach((file, index) => {
            const fsStream = createReadStream(file);
            console.log(`Copy to : ${fileDestinations[index]}`);
            fsStream.pipe(createWriteStream(fileDestinations[index]));
            fsStream.on('error', function(error) {
                complete(error,file);
            });
            fsStream.on('end', function() {
                complete(null,file);
            });
        });
    });
};

const deleteFiles = (targetFiles) => {
    return new Promise((resolve, reject) => {
        let errorCount = 0;
        let completeCount = 0;
        const checkComplete = (err) =>{
              if(err) errorCount++;
              completeCount++;
              if(completeCount === targetFiles.length) {
                  if (errorCount === 0)
                      resolve();
                  else
                      reject("error deleting files");
              }
        };
        targetFiles.forEach((targetFile) =>{
            access(targetFile, W_OK, (err) => {
               if(err) {
                   if(err.code === "ENOENT") //does not exist
                       checkComplete();
                   else {
                       console.log(err);
                       checkComplete(err);
                   }
               }
                else
                   unlink(targetFile, checkComplete);
            });
        });
        if(targetFiles.length === 0)
            resolve();
    });

};

module.exports = { readFiles, writeFiles, deleteFiles, createDir };