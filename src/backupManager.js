const { getNameDaily, getNameWeekly, getMonthName } = require("./calcName");
const { getManagerDrive } = require("./driveList");
const { readFiles, writeFiles, createDir } = require("./fileIo");
const { removeDirectories } = require("./fileRemoveDirectories");

const getBackUpFolders = (date, destination) => {
    const daily = getNameDaily(date);
    const dayDir = `/${destination}/${daily.name}`;
    const weekly = getNameWeekly(date);
    const weekDir = `/${destination}/${weekly.name}`;
    const monthDir = `/${destination}/${getMonthName(date)}`;
    return {
        dayDir,
        dayDeletePattern: daily.deletePattern,
        weekDir,
        weekDeletePattern: weekly.deletePattern,
        monthDir};
};

const calcReadFileNames = (sourceFileList, sourceDir, destinationDir) => {
    const sourceList = sourceFileList.map((sourceFile) =>{
        return `${sourceDir}/${sourceFile}`;
    });
    const destinationList = sourceFileList.map((sourceFile) =>{
        return `${destinationDir}/${sourceFile}`;
    });
    return {sourceList, destinationList};
};

const backUp = async (source)=> {
    let completed = false;
    try {
        const destinationDrive = await getManagerDrive();
        if (destinationDrive.hasResult === true) {
            const destBackups = getBackUpFolders(new Date(),
                `${destinationDrive.defaultFolder}/`);
            const deleteList = await readFiles(`${destinationDrive.drive}${destinationDrive.defaultFolder}`,
                `${destBackups.dayDeletePattern}|${destBackups.weekDeletePattern}`);
            const deleteFileList = deleteList.map((file) => { return `${destinationDrive.drive}${destinationDrive.defaultFolder}/${file}` });
            await removeDirectories(deleteFileList);

            await createDir(`${destinationDrive.drive}${destinationDrive.defaultFolder}`);
            const fileList = await readFiles(source, /.*\.(db)/);

            await writeBackUps(fileList, source, `${destinationDrive.drive}/${destBackups.dayDir}/`);
            await writeBackUps(fileList, source, `${destinationDrive.drive}/${destBackups.weekDir}/`);
            await writeBackUps(fileList, source, `${destinationDrive.drive}/${destBackups.monthDir}/`);
            completed = true;
        }
    } catch (ex) { console.log(ex) }
    return completed;
};

const writeBackUps = async (fileList, source, destinationFolder) => {
    await createDir(destinationFolder);
    const readWriteList = calcReadFileNames(fileList,
        `${source}/`, destinationFolder);
    await writeFiles(readWriteList.sourceList, readWriteList.destinationList);
};

module.exports = {getBackUpFolders, runBackUp:backUp};
