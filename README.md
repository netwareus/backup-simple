# backup-simple
----
Backup rotation daily, weekly, monthly for small file footprint.

## Install
----
npm install backup-simple

## Description
----

The back up looks for an attached memory stick, creates a backup directory (backup) then copies the files three times to a daily, weekly, and monthly folder.

The files are over written so that there are


* 7 daily folders
* 5 weekly folders
* One monthly folder for month in the year.


So in affect current week each day, current month each week, and one per month for ever.


## Sample Folders
----

On March 1 2018 the following folders are created:

* /backup/2018\_03
* /backup/2018\_03_week01
* /backup/2018\_03\_01_thursday

On March 2 2018

* /backup/2018_03 (over written)
* /backup/2018\_03\_week01 (over written)
* /backup/2018\_03\_01\_thursday (unchanged)
* /backup/2018\_03\_02\_friday (new)

One week later March 8 2018

* /backup/2018_03 (over written)
* /backup/2018\_03\_week01
* /backup/2018\_03\_week02 (new)
* /backup/2018\_03\_08\_thursday (new)
* /backup/2018\_03\_02\_friday
* /backup/2018\_03\_03\_saturday
* /backup/2018\_03\_04\_sunday
* /backup/2018\_03\_05\_monday
* /backup/2018\_03\_06\_tuesday
* /backup/2018\_03\_07\_wendsday


## Sample Code
----

```javascript
const { runBackUp } = require("backup-simple");
runBackUp("./test/testFiles/source/").then(function(complete) {
    console.log(complete);
});
```
